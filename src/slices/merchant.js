import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  fetchSellerOrderStatistic,
  fetchSellerOrders,
  fetchSellerProducts,
} from "../utils/const";

const initialState = {
  statistics: null,
  merchantOrders: {},
  merchantProducts: {},
  dashboard: {
    pollInterval: "this week",
    latestOrders: [],
  },
  product: {
    edit: false,
    editTarget: null,
    delete: false,
  },
};

export const getMerchantStatistics = createAsyncThunk(
  "merchant/getMerchantStatistics",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await fetchSellerOrderStatistic(
        payload.id,
        payload.query
      );
      return response;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getMerchantOrders = createAsyncThunk(
  "merchant/getMerchantOrders",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await fetchSellerOrders(payload.id, payload.query || {});
      return response;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getMerchantProducts = createAsyncThunk(
  "merchant/getMerchantProducts",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await fetchSellerProducts(
        payload.id,
        payload.query || {}
      );
      return response;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const merchantSlice = createSlice({
  name: "merchant",
  initialState,
  reducers: {
    setPollInterval: (state, pollInterval) => {
      state.dashboard.pollInterval = pollInterval.payload;
    },
    setLatestOrder: (state, latestOrder) => {
      state.dashboard.latestOrders = [
        ...state.dashboard.latestOrders,
        ...latestOrder.payload,
      ];
    },
    resetMerchantStatistics: (state) => {
      state.statistics = null;
    },
    openProductEdit: (state, id) => {
      state.product.edit = true;
      state.product.editTarget = id.payload;
    },
    closeProductEdit: (state) => {
      state.product.edit = false;
    },
    toggleProductDelete: (state) => {
      state.product.delete = !state.product.delete;
    },
  },
  extraReducers: {
    [getMerchantStatistics.rejected]: (state, action) => {
      state.statistics = {};
    },
    [getMerchantStatistics.fulfilled]: (state, action) => {
      state.statistics = action.payload;
    },
    [getMerchantOrders.fulfilled]: (state, action) => {
      state.merchantOrders = action.payload;
    },
    [getMerchantProducts.fulfilled]: (state, action) => {
      state.merchantProducts = action.payload;
      console.log(state.merchantProducts);
    },
  },
});

export const {
  setPollInterval,
  resetMerchantStatistics,
  setLatestOrder,
  toggleProductDelete,
  openProductEdit,
  closeProductEdit,
} = merchantSlice.actions;

export const selectMerchantStatistics = (state) => state.merchant.statistics;
export const selectDashboardPollInterval = (state) =>
  state.merchant.dashboard.pollInterval;
export const selectMerchantOrders = (state) => state.merchant.merchantOrders;
export const selectMerchantLatestOrders = (state) =>
  state.merchant.dashboard.latestOrders;
export const selectMerchantProducts = (state) =>
  state.merchant.merchantProducts;
export const selectProductEdit = (state) => state.merchant.product.edit;
export const selectProductDelete = (state) => state.merchant.product.delete;
export const selectProductEditTarget = (state) =>
  state.merchant.product.editTarget;

export default merchantSlice.reducer;
