import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchAuthenticatedUser, login, logout } from "../utils/const";

const initialState = {
  user: null,
  errorMessage: {
    header: "",
    content: "",
  },
};

export const checkAuth = createAsyncThunk(
  "auth/checkAuth",
  async (arg, { rejectWithValue }) => {
    try {
      const response = await fetchAuthenticatedUser();
      console.log(response);
      return response;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const loginWithRedirect = createAsyncThunk(
  "auth/login",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await login(payload.loginCredentials);
      return {
        user: response.user,
        redirectCallback: payload.redirectCallback,
      };
    } catch (error) {
      return rejectWithValue(error.response.status);
    }
  }
);

export const logoutWithRedirect = createAsyncThunk(
  "auth/logout",
  async (payload) => {
    return logout(payload.token).then((response) => {
      return {
        redirectCallback: payload.redirectCallback,
      };
    });
  }
);

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    clearErrorMessage: (state) => {
      state.errorMessage = {
        ...state.errorMessage,
        header: "",
        content: "",
      };
    },
  },
  extraReducers: {
    [checkAuth.rejected]: (state, action) => {
      state.user = null;
    },
    [checkAuth.fulfilled]: (state, action) => {
      state.user = action.payload.user;
    },
    [loginWithRedirect.fulfilled]: (state, action) => {
      state.user = action.payload.user;
      if (action.payload.user.roles[0].name === "merchant") {
        action.payload.redirectCallback("/merchant");
      } else {
        action.payload.redirectCallback("/admin");
      }
    },
    [loginWithRedirect.rejected]: (state, action) => {
      if (action.payload === 403) {
        state.errorMessage = {
          ...state.errorMessage,
          header: "Unauthorized action",
          content: "Please enter the correct login credentials",
        };
      } else if (action.payload === 500) {
        state.errorMessage = {
          ...state.errorMessage,
          header: "Unable to login",
          content: "Please enter the correct login credentials",
        };
      }
    },
    [logoutWithRedirect.fulfilled]: (_, action) => {
      action.payload.redirectCallback();
    },
  },
});

export const { clearErrorMessage } = authSlice.actions;

export const selectUser = (state) => state.auth.user;
export const selectAuthErrorMessage = (state) => state.auth.errorMessage;

export default authSlice.reducer;
