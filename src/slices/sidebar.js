import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  visible: false,
};

export const sidebarSlice = createSlice({
  name: "sidebar",
  initialState,
  reducers: {
    toggle: (state) => {
      state.visible = !state.visible;
    },
  },
});

export const { toggle } = sidebarSlice.actions;

export const selectVisible = (state) => state.sidebar.visible;

export default sidebarSlice.reducer;
