import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./slices/auth";
import sidebarReducer from "./slices/sidebar";
import merchantReducer from "./slices/merchant";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    sidebar: sidebarReducer,
    merchant: merchantReducer,
  },
});
