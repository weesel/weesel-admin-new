import React, { useState } from "react";
import { Button, Modal } from "@themesberg/react-bootstrap";
function ModalBox({ title, body, primaryBtnLabel, onClickHandler }) {
  const [showDefault, setShowDefault] = useState(true);
  const handleClose = () => setShowDefault(false);
  return (
    <React.Fragment>
      <Button
        style={{
          whiteSpace: "nowrap",
        }}
        variant="primary"
        className="my-3"
        onClick={() => setShowDefault(true)}
      >
        Create Product
      </Button>

      <Modal as={Modal.Dialog} centered show={showDefault} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title className="h6">{title}</Modal.Title>
          <Button variant="close" aria-label="Close" onClick={handleClose} />
        </Modal.Header>
        <Modal.Body>{body}</Modal.Body>
        <Modal.Footer>
          <Button
            variant="link"
            className="text-gray ms-auto"
            onClick={handleClose}
          >
            Close
          </Button>
          <Button variant="secondary" onClick={onClickHandler}>
            {primaryBtnLabel}
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
}

export default ModalBox;
