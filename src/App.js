import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { selectUser, checkAuth } from "./slices/auth";

/**
 * pages import
 */
import Login from "./pages/Login";
import Merchant from "./pages/Merchant";
import Admin from "./pages/Admin";
// core styles
import "./scss/volt.scss";

// vendor styles
import "@fortawesome/fontawesome-free/css/all.css";
import "react-datetime/css/react-datetime.css";
/**
 * component import
 */
import Sidebar from "./components/Sidebar";
import Navbar from "./components/Navbar";
import { selectProductEdit } from "./slices/merchant";

function App() {
  const user = useSelector(selectUser);
  const dispatch = useDispatch();
  const editProduct = useSelector(selectProductEdit);

  useEffect(() => {
    dispatch(checkAuth());
  }, []);

  return (
    <div className="App">
      <BrowserRouter forceRefresh={true}>
        {/* side bar here */}
        <Sidebar />
        <main className="content">
          <Navbar />
          <Switch>
            <Route exact path="/">
              <Login />
            </Route>
            {user && user.roles[0].name === "admin" ? (
              <Route path="/admin">
                <Admin />
              </Route>
            ) : (
              <Route path="/merchant">
                <Merchant />
              </Route>
            )}
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
