import axios from "axios";

export const login = async (payload) => {
  try {
    const response = await axios.post("/auth/admin/login", payload, {
      withCredentials: true,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const logout = async () => {
  try {
    const response = await axios.post("/auth/admin/logout", null);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const fetchAuthenticatedUser = async () => {
  try {
    const response = await axios.get("/auth/me", {
      withCredentials: true,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const fetchCategoriesWithChildren = async () => {
  return axios
    .get("/categories", {
      params: {
        include: "children",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const fetchCategoryWithChildren = async (id) => {
  return axios
    .get(`/categories/${id}`, {
      params: {
        include: "children",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const fetchSellerOrderStatistic = async (id, query) => {
  return axios
    .get(`/sellers/${id}/orders/statistics`, {
      params: query,
    })
    .then((response) => response.data)
    .catch((error) => error);
};

export const fetchSellerOrders = async (id, query) => {
  return axios
    .get(`/sellers/${id}/orders`, {
      params: query,
    })
    .then((response) => response.data)
    .catch((error) => error);
};

export const fetchSellerProducts = async (id, query) => {
  return axios
    .get(`/sellers/${id}/products`, {
      params: query,
    })
    .then((response) => response.data)
    .catch((error) => error);
};
