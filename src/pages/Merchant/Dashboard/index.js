import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCashRegister,
  faClipboardCheck,
  faTruck,
  faClock,
  faChartLine,
  faCloudUploadAlt,
  faPlus,
  faRocket,
  faTasks,
  faUserShield,
  faStore,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Button,
  Dropdown,
  ButtonGroup,
} from "@themesberg/react-bootstrap";

import {
  CounterWidget,
  CircleChartWidget,
  BarChartWidget,
  TeamMembersWidget,
  ProgressTrackWidget,
  RankingWidget,
  SalesValueWidget,
  SalesValueWidgetPhone,
  AcquisitionWidget,
} from "../../../components/Widgets";
import { PageVisitsTable } from "../../../components/Tables";
import Preloader from "../../../components/Preloader";
import { trafficShares, totalOrders } from "../../../data/charts";
import { useSelector } from "react-redux";
import {
  selectMerchantStatistics,
  getMerchantStatistics,
  selectDashboardPollInterval,
  resetMerchantStatistics,
  selectMerchantOrders,
  getMerchantOrders,
  selectMerchantLatestOrders,
  setLatestOrder,
} from "../../../slices/merchant";
import { useDispatch } from "react-redux";
import { selectUser } from "../../../slices/auth";
import { TotalRevenue, TotalRevenuePhone } from "./TotalRevenue";
import { OrdersTableMini } from "../Order/OrdersTable";
import CompareRevenue from "./CompareRevenue";

function Dashboard() {
  const user = useSelector(selectUser);
  const pollInterval = useSelector(selectDashboardPollInterval);
  const merchantStatistics = useSelector(selectMerchantStatistics);
  const merchantOrders = useSelector(selectMerchantOrders);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(resetMerchantStatistics());
  }, [pollInterval]);
  useEffect(() => {
    if (user && merchantStatistics === null) {
      dispatch(
        getMerchantStatistics({
          id: user.id,
          query: {
            date: pollInterval,
          },
        })
      );
    }
  }, [user, merchantStatistics]);

  useEffect(() => {
    if (user && Object.keys(merchantOrders).length === 0) {
      dispatch(
        getMerchantOrders({
          id: user.id,
          query: { limit: 5 },
        })
      );
    }
  }, [user, merchantOrders]);

  return (
    <React.Fragment>
      <Preloader show={merchantStatistics ? false : true} />
      <Row className="justify-content-md-center pt-4">
        <Col xs={12} className="mb-4 d-none d-sm-block">
          <TotalRevenue />
        </Col>
        <Col xs={12} className="mb-4 d-sm-none">
          <TotalRevenuePhone />
        </Col>
        <Col xs={12} sm={6} xl={4} className="mb-4">
          {/* implement in-stock product count */}
          <CounterWidget
            category="In-stock Products"
            title={
              (merchantStatistics && merchantStatistics.in_stock_product) || 0
            }
            icon={faStore}
            iconColor="shape-secondary"
          />
        </Col>

        <Col xs={12} sm={6} xl={4} className="mb-4">
          <CounterWidget
            category="Total Revenue"
            title={`$ ${
              (merchantStatistics && merchantStatistics.total_revenue) || 0
            }`}
            icon={faCashRegister}
            iconColor="shape-tertiary"
          />
        </Col>

        <Col xs={12} sm={6} xl={4} className="mb-4">
          {merchantStatistics && (
            <CircleChartWidget
              title="Orders"
              data={[
                {
                  id: 1,
                  label: "Completed",
                  value: (
                    (merchantStatistics.completed_orders /
                      merchantStatistics.total_orders) *
                    100
                  ).toFixed(2),
                  color: "primary",
                  icon: faClipboardCheck,
                },
                {
                  id: 2,
                  label: "Delivered",
                  value: (
                    (merchantStatistics.delivered_orders /
                      merchantStatistics.total_orders) *
                    100
                  ).toFixed(2),
                  icon: faTruck,
                  color: "secondary",
                },
                {
                  id: 2,
                  label: "Pending",
                  value: (
                    (merchantStatistics.pending_orders /
                      merchantStatistics.total_orders) *
                    100
                  ).toFixed(2),
                  icon: faClock,
                  color: "tertiary",
                },
              ]}
            />
          )}
        </Col>
      </Row>

      <Row>
        <Col xs={12} xl={12} className="mb-4">
          <Row>
            <Col xs={12} xl={8} className="mb-4">
              <Row>
                <Col xs={12} className="mb-4">
                  <OrdersTableMini title="Lastest Orders" />
                </Col>
              </Row>
            </Col>

            <Col xs={12} xl={4}>
              <Row>
                <Col xs={12} className="mb-4">
                  {/* get total revenue for two months and compare them */}
                  <CompareRevenue />
                  {/* <BarChartWidget
                    title="Total orders"
                    value={452}
                    percentage={18.2}
                    data={totalOrders}
                  /> */}
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  );
}

export default Dashboard;
