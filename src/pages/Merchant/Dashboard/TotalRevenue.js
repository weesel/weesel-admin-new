import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faChartArea,
  faChartBar,
  faChartLine,
  faFlagUsa,
  faFolderOpen,
  faGlobeEurope,
  faPaperclip,
  faUserPlus,
} from "@fortawesome/free-solid-svg-icons";
import {
  faAngular,
  faBootstrap,
  faReact,
  faVuejs,
} from "@fortawesome/free-brands-svg-icons";
import {
  Col,
  Row,
  Card,
  Image,
  Button,
  ListGroup,
  ProgressBar,
} from "@themesberg/react-bootstrap";

import {
  SalesValueChart,
  SalesValueChartphone,
} from "../../../components/Charts";
import { useSelector, useDispatch } from "react-redux";
import {
  selectDashboardPollInterval,
  selectMerchantStatistics,
  setPollInterval,
} from "../../../slices/merchant";
import { capitalizeFirstLetter } from "../../../utils/const";

export const TotalRevenue = () => {
  const dispatch = useDispatch();

  const pollInterval = useSelector(selectDashboardPollInterval);
  const merchantStatistics = useSelector(selectMerchantStatistics);

  const [chartLabels, setChartLabels] = useState([]);
  const [chartValues, setChartValues] = useState([]);

  const [earningToday, setEarningToday] = useState(0);

  useEffect(() => {
    if (merchantStatistics) {
      setChartLabels(Object.keys(merchantStatistics.total_revenue_statistic));
      setChartValues(Object.values(merchantStatistics.total_revenue_statistic));

      setEarningToday(merchantStatistics.earning_today);
    }
  }, [merchantStatistics]);

  return (
    <Card className="bg-secondary-alt shadow-sm">
      <Card.Header className="d-flex flex-row align-items-center flex-0">
        <div className="d-block">
          <h5 className="fw-normal mb-2">Your Earning Today</h5>
          <h3>${earningToday}</h3>
        </div>
        <div className="d-flex ms-auto">
          <Button
            onClick={() => dispatch(setPollInterval("this month"))}
            variant={pollInterval === "this month" ? "primary" : "secondary"}
            size="sm"
            className="me-2"
          >
            Month
          </Button>
          <Button
            onClick={() => dispatch(setPollInterval("this week"))}
            variant={pollInterval === "this week" ? "primary" : "secondary"}
            size="sm"
            className="me-3"
          >
            Week
          </Button>
        </div>
      </Card.Header>
      <Card.Body className="p-2">
        <SalesValueChart labels={chartLabels} series={chartValues} />
      </Card.Body>
    </Card>
  );
};

export const TotalRevenuePhone = () => {
  const dispatch = useDispatch();

  const pollInterval = useSelector(selectDashboardPollInterval);
  const merchantStatistics = useSelector(selectMerchantStatistics);

  const [chartLabels, setChartLabels] = useState([]);
  const [chartValues, setChartValues] = useState([]);

  const [earningToday, setEarningToday] = useState(0);

  useEffect(() => {
    if (merchantStatistics) {
      setChartLabels(Object.keys(merchantStatistics.total_revenue_statistic));
      setChartValues(Object.values(merchantStatistics.total_revenue_statistic));

      setEarningToday(merchantStatistics.earning_today);
    }
  }, [merchantStatistics]);
  return (
    <Card className="bg-secondary-alt shadow-sm">
      <Card.Header className="d-md-flex flex-row align-items-center flex-0">
        <div className="d-block mb-3 mb-md-0">
          <h5 className="fw-normal mb-2">Your Earning Today</h5>
          <h3>${earningToday}</h3>
        </div>
        <div className="d-flex ms-auto">
          <Button
            onClick={() => dispatch(setPollInterval("month"))}
            variant={pollInterval === "month" ? "primary" : "secondary"}
            size="sm"
            className="me-2"
          >
            Month
          </Button>
          <Button
            onClick={() => dispatch(setPollInterval("month"))}
            variant={pollInterval === "month" ? "primary" : "secondary"}
            size="sm"
            className="me-3"
          >
            Week
          </Button>
        </div>
      </Card.Header>
      <Card.Body className="p-2">
        <SalesValueChartphone labels={chartLabels} series={chartValues} />
      </Card.Body>
    </Card>
  );
};
