import React, { useEffect, useState } from "react";
import { BarChart } from "../../../components/Charts";
import { Card } from "@themesberg/react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fetchSellerOrderStatistic } from "../../../utils/const";
import { useSelector } from "react-redux";
import { selectUser } from "../../../slices/auth";

function CompareRevenue() {
  const user = useSelector(selectUser);
  const [thisMonthRevenue, setThisMonthRevenue] = useState({});
  const [lastMonthRevenue, setLastMonthRevenue] = useState({});
  const [labels, setLabels] = useState([]);

  useEffect(() => {
    if (user && Object.keys(thisMonthRevenue).length === 0) {
      fetchSellerOrderStatistic(user.id, {
        exclude:
          "total_orders,total_revenue,payment_statistics,earning_today,in_stock_product,pending_orders,delivered_orders,completed_orders,seller",
        date: "this month",
      })
        .then(({ total_revenue_statistic }) =>
          setThisMonthRevenue(total_revenue_statistic)
        )
        .catch((error) => console.log(error));
    }
    if (user && Object.keys(lastMonthRevenue).length === 0) {
      fetchSellerOrderStatistic(user.id, {
        exclude:
          "total_orders,total_revenue,payment_statistics,earning_today,in_stock_product,pending_orders,delivered_orders,completed_orders,seller",
        date: "last month",
      })
        .then(({ total_revenue_statistic }) =>
          setLastMonthRevenue(total_revenue_statistic)
        )
        .catch((error) => console.log(error));
    }

    if (
      Object.keys(thisMonthRevenue).length > 0 &&
      Object.keys(lastMonthRevenue).length > 0
    ) {
      setLabels((prevState) => {
        prevState = Object.keys(thisMonthRevenue).concat(
          Object.keys(lastMonthRevenue).filter(
            (value) => !Object.keys(thisMonthRevenue).includes(value)
          )
        );
        return prevState;
      });
    }
  }, [user, thisMonthRevenue, lastMonthRevenue]);

  return (
    <Card border="light" className="shadow-sm">
      <Card.Body className="d-flex flex-row align-items-center flex-0 border-bottom">
        <div className="d-block">
          <h6 className="fw-normal text-gray mb-2">Revenue Comparison</h6>
        </div>
        <div className="d-block ms-auto">
          <div
            key={`bar-element-1`}
            className="d-flex align-items-center text-end mb-2"
          >
            <span
              style={{ backgroundColor: "#1B998B" }}
              className={`shape-xs rounded-circle me-2`}
            />
            <small className="fw-normal">Last Month</small>
          </div>
          <div
            key={`bar-element-2`}
            className="d-flex align-items-center text-end mb-2"
          >
            <span
              style={{ backgroundColor: "#17a5ce" }}
              className={`shape-xs rounded-circle me-2`}
            />
            <small className="fw-normal">This Month</small>
          </div>
        </div>
      </Card.Body>
      <Card.Body className="p-2">
        {Object.keys(thisMonthRevenue).length > 0 &&
          Object.keys(lastMonthRevenue).length > 0 && (
            <BarChart
              labels={labels}
              series={[
                Object.values(lastMonthRevenue),
                Object.values(thisMonthRevenue),
              ]}
            />
          )}
      </Card.Body>
    </Card>
  );
}

export default CompareRevenue;
