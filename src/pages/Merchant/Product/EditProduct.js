import React, { useEffect, useState, useRef } from "react";
import { closeProductEdit } from "../../../slices/merchant";
import {
  Col,
  Row,
  Card,
  Form,
  Button,
  InputGroup,
} from "@themesberg/react-bootstrap";
import CustomerCategorySelector from "./SelectCategory";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { selectUser } from "../../../slices/auth";
import nProgress from "nprogress";
import { useLocation } from "react-router-dom";
import { selectProductEditTarget } from "../../../slices/merchant";
function EditProduct() {
  const dispatch = useDispatch();
  const editTarget = useSelector(selectProductEditTarget);

  useEffect(() => {
    document.querySelector("#root").style.height = 0;
    document.querySelector(".App").style.height = 0;
  }, []);

  useEffect(() => {}, [editTarget]);

  const user = useSelector(selectUser);
  // product name
  const [name, setName] = useState("");
  // product description
  const [description, setDescription] = useState("");
  // product quantity
  const [quantity, setQuantity] = useState("");
  // product images
  const [images, setImages] = useState(null);
  // product price
  const [price, setPrice] = useState(0);
  // product discount
  const [discount, setDiscount] = useState(0);
  /**
   * product categories
   */
  const [categories, setCategories] = useState([]);
  const addCategories = (item) => {
    setCategories((prevState) => {
      prevState.push(item);
      return prevState;
    });
  };
  const removeCategories = (item) => {
    setCategories((prevState) => {
      prevState.splice(prevState.indexOf(item), 1);
      return prevState;
    });
  };
  const fileInput = useRef();

  const resetForm = () => {
    setName("");
    setDescription("");
    setQuantity("");
    setPrice(0);
    setDiscount(0);
    setCategories([]);
    setImages(null);
    fileInput.current.value = "";
  };

  const submitHandler = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description || "n/a");
    formData.append("quantity", parseInt(quantity));
    formData.append("discount", parseInt(discount));
    formData.append("price", parseInt(price));
    categories.map((category) =>
      formData.append("category_ids[]", parseInt(category))
    );
    if (images) {
      images.map((image) => formData.append("images[]", image));
    }
    nProgress.start();
    axios
      .post("/products", formData, {
        withCredentials: true,
      })
      .then((response) => {
        console.log(response);
        // resetForm();
        nProgress.done();
      })
      .catch((error) => console.log(error.response));
  };

  return (
    <div
      className="editProduct animate__animated animate__jackInTheBox"
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        zIndex: 9999,
        minHeight: "100vh",
        width: "100%",
        background: "white",
      }}
    >
      <div className="d-flex justify-content-end p-3">
        <Button onClick={() => dispatch(closeProductEdit())}>Close</Button>
      </div>
      {/* main content here */}
      <Card border="light" className="bg-white shadow-sm mb-4">
        <Card.Body>
          <h5 className="mb-4">Product Information</h5>
          <Form>
            <Row>
              <Col md={12} className="mb-3">
                <Form.Group id="productName">
                  <Form.Label>Product name</Form.Label>
                  <Form.Control
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                    type="text"
                    placeholder="Enter your product name"
                  />
                </Form.Group>
              </Col>
              <Col md={12} className="mb-3">
                <Form.Group id="productDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    required
                    placeholder="How do you describe your product?"
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row className="align-items-center">
              <Col md={4} className="mb-3">
                <Form.Group id="productPrice">
                  <Form.Label>Price</Form.Label>
                  <InputGroup>
                    <Form.Control
                      type="text"
                      required
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                      defaultValue={0}
                    />
                    <InputGroup.Text>$</InputGroup.Text>
                  </InputGroup>
                </Form.Group>
              </Col>
              <Col md={4} className="mb-3">
                <Form.Group id="productDiscount">
                  <Form.Label>Discount</Form.Label>
                  <InputGroup>
                    <Form.Control
                      type="text"
                      required
                      value={discount}
                      onChange={(e) => setDiscount(e.target.value)}
                    />
                    <InputGroup.Text>%</InputGroup.Text>
                  </InputGroup>
                </Form.Group>
              </Col>
              <Col md={4} className="mb-3">
                <Form.Group id="productInStock">
                  <Form.Label>In Stock</Form.Label>
                  <Form.Control
                    value={quantity}
                    onChange={(e) => setQuantity(e.target.value)}
                    type="text"
                    required
                    defaultValue={0}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={6} className="mb-3">
                <Form.Group id="productImages" className="mb-3">
                  <Form.Label>Images</Form.Label>
                  <Form.Control
                    type="file"
                    multiple
                    ref={fileInput}
                    onChange={(e) => setImages(Array.from(e.target.files))}
                  />
                </Form.Group>
              </Col>
              <Col md={6} className="mb-3">
                <Form.Group id="Choose Category"></Form.Group>
              </Col>
            </Row>
            <h5 className="my-4">Select Category</h5>
            <Row>
              <Col sm={9} className="mb-3">
                <Form.Group id="productCategories">
                  <CustomerCategorySelector
                    state={categories}
                    add={addCategories}
                    remove={removeCategories}
                  />
                </Form.Group>
              </Col>
            </Row>
            <div className="mt-3">
              <Button onClick={submitHandler} variant="primary" type="submit">
                Create
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}

export default EditProduct;
