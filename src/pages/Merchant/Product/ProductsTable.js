import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faArrowDown,
  faArrowUp,
  faEdit,
  faEllipsisH,
  faExternalLinkAlt,
  faEye,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
} from "@themesberg/react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import { pageVisits, pageTraffic, pageRanking } from "../../../data/tables";
import { useSelector } from "react-redux";
import { selectUser } from "../../../slices/auth";
import { useDispatch } from "react-redux";
import queryString from "query-string";
import EditProduct from "./EditProduct";
import {
  getMerchantProducts,
  selectMerchantProducts,
  openProductEdit,
  selectProductEdit,
} from "../../../slices/merchant";

function ProductsTable() {
  const user = useSelector(selectUser);
  const merchantProducts = useSelector(selectMerchantProducts);
  const editProduct = useSelector(selectProductEdit);
  const dispatch = useDispatch();

  const { search, pathname } = useLocation();
  const { page } = queryString.parse(search);

  useEffect(() => {
    if (user) {
      dispatch(
        getMerchantProducts({
          id: user.id,
          query: {
            page: page,
          },
        })
      );
    }
  }, [user]);

  const TableRow = (props) => {
    const {
      id,
      name,
      status,
      price,
      categories,
      quantity,
      sale_count,
      images,
    } = props;

    return (
      <tr>
        <td>
          <Card.Link href="#" className="text-primary fw-bold">
            {id}
          </Card.Link>
        </td>
        <td className="fw-bold">{name}</td>
        <td>{status}</td>
        <td>{price}</td>
        <td>{categories.map((category) => category.name).join(",")}</td>
        <td>{quantity}</td>
        <td>{sale_count}</td>
        <td style={{ maxWidth: "100px" }}>
          <Image responsive src={`${images[0].url}`} fluid />
        </td>
        <td>
          <Dropdown as={ButtonGroup}>
            <Dropdown.Toggle
              as={Button}
              split
              variant="link"
              className="text-dark m-0 p-0"
            >
              <span className="icon icon-sm">
                <FontAwesomeIcon icon={faEllipsisH} className="icon-dark" />
              </span>
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item>
                <FontAwesomeIcon icon={faEye} className="me-2" /> View Details
              </Dropdown.Item>
              <Dropdown.Item onClick={() => dispatch(openProductEdit(id))}>
                <FontAwesomeIcon icon={faEdit} className="me-2" /> Edit
              </Dropdown.Item>
              <Dropdown.Item className="text-danger">
                <FontAwesomeIcon icon={faTrashAlt} className="me-2" /> Remove
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </td>
      </tr>
    );
  };

  return (
    <React.Fragment>
      {editProduct && <EditProduct />}
      {/* <EditProduct /> */}
      <Card border="light" className="shadow-sm mb-4">
        <Card.Body className="pb-0">
          <Table
            responsive
            className="table-centered table-nowrap rounded mb-0"
          >
            <thead className="thead-light">
              <tr>
                <th className="border-0">ID</th>
                <th className="border-0">Name</th>
                <th className="border-0">Status</th>
                <th className="border-0">Price</th>
                <th className="border-0">Categories</th>
                <th className="border-0">In-Stock</th>
                {/* sales count */}
                <th className="border-0">Sold</th>
                <th className="border-0">Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(merchantProducts).length > 0 &&
                merchantProducts.products.data.map((product) => (
                  <TableRow key={`product-${product.id}`} {...product} />
                ))}
            </tbody>
          </Table>
        </Card.Body>
        <Card.Footer className="px-3 border-0 d-lg-flex align-items-center justify-content-between">
          <Nav>
            <Pagination className="mb-2 mb-lg-0">
              {Object.keys(merchantProducts).length > 0 &&
                merchantProducts.products.links.map((link) => {
                  if (link.label.toLowerCase().includes("next")) {
                    return (
                      <Pagination.Next
                        href={
                          link.url
                            ? `${pathname}?${link.url.split("?")[1]}`
                            : null
                        }
                        disabled={link.url === null}
                        active={link.active}
                      >
                        Next
                      </Pagination.Next>
                    );
                  } else if (link.label.toLowerCase().includes("prev")) {
                    return (
                      <Pagination.Prev
                        href={
                          link.url
                            ? `${pathname}?${link.url.split("?")[1]}`
                            : null
                        }
                        disabled={link.url === null}
                        active={link.active}
                      >
                        Previous
                      </Pagination.Prev>
                    );
                  } else {
                    return (
                      <Pagination.Item
                        href={
                          link.url
                            ? `${pathname}?${link.url.split("?")[1]}`
                            : null
                        }
                        active={link.active}
                        disabled={link.url === null}
                      >
                        {link.label}
                      </Pagination.Item>
                    );
                  }
                })}
            </Pagination>
          </Nav>
        </Card.Footer>
      </Card>
    </React.Fragment>
  );
}

export const RankingTable = () => {
  const TableRow = (props) => {
    const {
      country,
      countryImage,
      overallRank,
      overallRankChange,
      travelRank,
      travelRankChange,
      widgetsRank,
      widgetsRankChange,
    } = props;

    return (
      <tr>
        <td className="border-0">
          <Card.Link href="#" className="d-flex align-items-center">
            <Image
              src={countryImage}
              className="image-small rounded-circle me-2"
            />
            <div>
              <span className="h6">{country}</span>
            </div>
          </Card.Link>
        </td>
        <td className="fw-bold border-0">{overallRank ? overallRank : "-"}</td>
        <td className="border-0">
          {/* <ValueChange value={overallRankChange} /> */}
        </td>
        <td className="fw-bold border-0">{travelRank ? travelRank : "-"}</td>
        <td className="border-0">
          {/* <ValueChange value={travelRankChange} /> */}
        </td>
        <td className="fw-bold border-0">{widgetsRank ? widgetsRank : "-"}</td>
        <td className="border-0">
          {/* <ValueChange value={widgetsRankChange} /> */}
        </td>
      </tr>
    );
  };

  return (
    <Card border="light" className="shadow-sm">
      <Card.Body className="pb-0">
        <Table responsive className="table-centered table-nowrap rounded mb-0">
          <thead className="thead-light">
            <tr>
              <th className="border-0">Country</th>
              <th className="border-0">All</th>
              <th className="border-0">All Change</th>
              <th className="border-0">Travel & Local</th>
              <th className="border-0">Travel & Local Change</th>
              <th className="border-0">Widgets</th>
              <th className="border-0">Widgets Change</th>
            </tr>
          </thead>
          <tbody>
            {pageRanking.map((r) => (
              <TableRow key={`ranking-${r.id}`} {...r} />
            ))}
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
};

export default ProductsTable;
