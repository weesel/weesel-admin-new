import React, { useEffect, useState, useRef } from "react";
import {
  Col,
  Row,
  Card,
  Form,
  Button,
  InputGroup,
} from "@themesberg/react-bootstrap";
import CustomerCategorySelector from "./SelectCategory";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { selectUser } from "../../../slices/auth";
import nProgress from "nprogress";
function CreateProduct() {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);
  // product name
  const [name, setName] = useState("");
  // product description
  const [description, setDescription] = useState("");
  // product quantity
  const [quantity, setQuantity] = useState("");
  // product images
  const [images, setImages] = useState(null);
  // product price
  const [price, setPrice] = useState(0);
  // product discount
  const [discount, setDiscount] = useState(0);
  /**
   * product categories
   */
  const [categories, setCategories] = useState([]);
  const addCategories = (item) => {
    setCategories((prevState) => {
      prevState.push(item);
      return prevState;
    });
  };
  const removeCategories = (item) => {
    setCategories((prevState) => {
      prevState.splice(prevState.indexOf(item), 1);
      return prevState;
    });
  };
  const fileInput = useRef();

  const resetForm = () => {
    setName("");
    setDescription("");
    setQuantity("");
    setPrice(0);
    setDiscount(0);
    setCategories([]);
    setImages(null);
    fileInput.current.value = "";
  };

  const submitHandler = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description || "n/a");
    formData.append("quantity", parseInt(quantity));
    formData.append("discount", parseInt(discount));
    formData.append("price", parseInt(price));
    categories.map((category) =>
      formData.append("category_ids[]", parseInt(category))
    );
    if (images) {
      images.map((image) => formData.append("images[]", image));
    }
    axios
      .post("/products", formData, {
        withCredentials: true,
      })
      .then((_response) => {
        window.reload();
      })
      .catch((error) => console.log(error.response));
  };

  return (
    <Card border="light" className="bg-white shadow-sm mb-4">
      <Card.Body>
        <h5 className="mb-4">Product Information</h5>
        <Form>
          <Row>
            <Col md={12} className="mb-3">
              <Form.Group id="productName">
                <Form.Label>Product name</Form.Label>
                <Form.Control
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  required
                  type="text"
                  placeholder="Enter your product name"
                />
              </Form.Group>
            </Col>
            <Col md={12} className="mb-3">
              <Form.Group id="productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  required
                  placeholder="How do you describe your product?"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row className="align-items-center">
            <Col md={4} className="mb-3">
              <Form.Group id="productPrice">
                <Form.Label>Price</Form.Label>
                <InputGroup>
                  <Form.Control
                    type="text"
                    required
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                    defaultValue={0}
                  />
                  <InputGroup.Text>$</InputGroup.Text>
                </InputGroup>
              </Form.Group>
            </Col>
            <Col md={4} className="mb-3">
              <Form.Group id="productDiscount">
                <Form.Label>Discount</Form.Label>
                <InputGroup>
                  <Form.Control
                    type="text"
                    required
                    value={discount}
                    onChange={(e) => setDiscount(e.target.value)}
                  />
                  <InputGroup.Text>%</InputGroup.Text>
                </InputGroup>
              </Form.Group>
            </Col>
            <Col md={4} className="mb-3">
              <Form.Group id="productInStock">
                <Form.Label>In Stock</Form.Label>
                <Form.Control
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value)}
                  type="text"
                  required
                  defaultValue={0}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col md={6} className="mb-3">
              <Form.Group id="productImages" className="mb-3">
                <Form.Label>Images</Form.Label>
                <Form.Control
                  type="file"
                  multiple
                  ref={fileInput}
                  onChange={(e) => setImages(Array.from(e.target.files))}
                />
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="Choose Category"></Form.Group>
            </Col>
          </Row>
          <h5 className="my-4">Select Category</h5>
          <Row>
            <Col sm={9} className="mb-3">
              <Form.Group id="productCategories">
                <CustomerCategorySelector
                  state={categories}
                  add={addCategories}
                  remove={removeCategories}
                />
              </Form.Group>
            </Col>
          </Row>
          <div className="mt-3">
            <Button onClick={submitHandler} variant="primary" type="submit">
              Create
            </Button>
          </div>
        </Form>
      </Card.Body>
    </Card>
  );
}

export default CreateProduct;
