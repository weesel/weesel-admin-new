import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Dropdown, Form, Icon, Image, Modal } from "semantic-ui-react";
import { selectConfigId } from "../../../slices/configMenuSlice";
import {
  addSelectedCategories,
  clearSelectedCategories,
  selectSelectedCategories,
} from "../../../slices/productSlice";
import SelectCategory from "../SelectCategory";

const fetchProduct = (id) => {
  return axios
    .get(`/products/${id}`)
    .then((response) => response.data.data)
    .catch((error) => error);
};

//eslint-disable-next-line
export default () => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [quantity, setQuantity] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [status, setStatus] = useState("");
  const [selectedCategories, setSelectedCategories] = useState();
  const [images, setImages] = useState([]);
  const configId = useSelector(selectConfigId);

  const addSelectedCategories = (toBeAdded) => {
    const temp = selectedCategories || [];
    temp.push(toBeAdded);
    setSelectedCategories(temp);
  };

  const removeSelectedCategories = (toBeRemoved) => {
    const temp = selectedCategories;
    temp.splice(temp.indexOf(toBeRemoved), 1);
    setSelectedCategories(temp);
  };

  useEffect(() => {
    fetchProduct(configId)
      .then((product) => {
        setName(product.name);
        setDescription(product.description);
        setQuantity(product.quantity);
        setDiscount(product.discount);
        setStatus(product.status);
        //images
        setImages(product.images);
        //categories
        setSelectedCategories(
          product.categories.map((category) => category.id + "")
        );
      })
      .catch((error) => console.log(error));
  }, []);

  const submitHandler = () => console.log("Hello World");
  return (
    <>
      <Modal.Header>Edit Category</Modal.Header>
      <Modal.Content>
        <Form onSubmit={submitHandler}>
          <Form.Field>
            <Form.Input
              label="Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Product Name"
            />
          </Form.Field>
          <Form.Field>
            <Form.TextArea
              label="Description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              placeholder="Product Description"
            />
          </Form.Field>
          <Form.Field>
            <Form.Input
              label="Quantity"
              value={quantity}
              onChange={(e) => setQuantity(e.target.value)}
              placeholder="Product Quantity"
            />
          </Form.Field>
          <Form.Field>
            <Form.Input
              label="Discount"
              value={discount}
              onChange={(e) => setDiscount(e.target.value)}
              placeholder="Product Discount"
            />
          </Form.Field>
          <Form.Field>
            <label>Status</label>
            <Dropdown
              placeholder="Availability"
              fluid
              value={status}
              selection
              onChange={(e, { value }) => setStatus(value)}
              options={[
                { text: "Available", value: "available" },
                { text: "Unavailable", value: "unavailable" },
              ]}
            />
          </Form.Field>
          <Form.Field>
            <label style={{ position: "sticky", top: "0" }}>Category: </label>
            <div
              style={{
                height: "200px",
                overflowY: "scroll",
                position: "relative",
              }}
            >
              {selectedCategories && (
                <SelectCategory
                  selectedCategories={selectedCategories}
                  addSelectedCategories={addSelectedCategories}
                  removeSelectedCategories={removeSelectedCategories}
                />
              )}
            </div>
          </Form.Field>
          <Form.Field>
            <label>Images: </label>
            <Image.Group size="tiny" style={{ display: "flex" }}>
              {images &&
                images.map((image) => (
                  <div
                    style={{
                      width: "fit-content",
                      position: "relative",
                      marginRight: "5rem",
                      height: "100px",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      flexWrap: "wrap",
                    }}
                  >
                    <Image src={image.url} />
                    <Button
                      icon
                      size="tiny"
                      circular
                      negative
                      style={{ position: "absolute", top: 0, right: "-3rem" }}
                    >
                      <Icon name="close"></Icon>
                    </Button>
                  </div>
                ))}
            </Image.Group>
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button type="positive" positive>
          Update
        </Button>
      </Modal.Actions>
    </>
  );
};
