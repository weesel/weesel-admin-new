import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faArrowDown,
  faArrowUp,
  faEdit,
  faEllipsisH,
  faExternalLinkAlt,
  faEye,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
} from "@themesberg/react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  selectMerchantOrders,
  selectMerchantLatestOrders,
  getMerchantOrders,
} from "../../../slices/merchant";
import { selectUser } from "../../../slices/auth";
import queryString from "query-string";
export const OrdersTableMini = ({ title }) => {
  const merchantOrders = useSelector(selectMerchantOrders);

  useEffect(() => {
    console.log("OrdersTableMini ", merchantOrders);
  }, [merchantOrders]);

  const TableRow = (props) => {
    const { id, name, total_cost, status } = props;
    return (
      <tr>
        <th scope="row">{id}</th>
        <td>{name}</td>
        <td>${total_cost}</td>
        <td>{status}</td>
      </tr>
    );
  };

  return (
    <Card border="light" className="shadow-sm">
      <Card.Header>
        <Row className="align-items-center">
          <Col>
            <h5>{title}</h5>
          </Col>
          <Col className="text-end">
            <Button
              href="/merchant/orders?page=1"
              variant="secondary"
              size="sm"
            >
              See all
            </Button>
          </Col>
        </Row>
      </Card.Header>
      <Table responsive className="align-items-center table-flush">
        <thead className="thead-light">
          <tr>
            <th scope="col">Order ID</th>
            <th scope="col">Receipient</th>
            <th scope="col">Total</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(merchantOrders).length > 0 &&
            merchantOrders.orders.map((order) => (
              <TableRow key={`order-${order.id}`} {...order} />
            ))}
        </tbody>
      </Table>
    </Card>
  );
};

export const OrdersTable = () => {
  const merchantOrders = useSelector(selectMerchantOrders);
  const user = useSelector(selectUser);
  const { search, pathname } = useLocation();
  const dispatch = useDispatch();
  const { page } = queryString.parse(search);

  useEffect(() => {
    if (user) {
      console.log(page);
      dispatch(
        getMerchantOrders({
          id: user.id,
          query: {
            page: page,
          },
        })
      );
    }
  }, [user]);

  const TableRow = (props) => {
    const {
      id,
      name,
      created_at,
      total_cost,
      status,
      payment_method,
      aba_transaction_id,
    } = props;
    const statusVariant =
      status === "completed"
        ? "success"
        : status === "pending"
        ? "warning"
        : status === "cancelled"
        ? "danger"
        : "primary";

    return (
      <tr>
        <td>
          <Card.Link as={Link} to={"/"} className="fw-normal">
            {id}
          </Card.Link>
        </td>
        <td>
          <span className="fw-normal">{name}</span>
        </td>
        <td>
          <span className="fw-normal">{created_at}</span>
        </td>
        <td>
          <span className="fw-normal">${total_cost}</span>
        </td>
        <td>
          <span className={`fw-normal text-${statusVariant}`}>{status}</span>
        </td>
        <td>
          <span className="fw-normal">{payment_method}</span>
        </td>
        <td>
          <span className="fw-normal">{aba_transaction_id}</span>
        </td>
        <td>
          <Dropdown as={ButtonGroup}>
            <Dropdown.Toggle
              as={Button}
              split
              variant="link"
              className="text-dark m-0 p-0"
            >
              <span className="icon icon-sm">
                <FontAwesomeIcon icon={faEllipsisH} className="icon-dark" />
              </span>
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item>
                <FontAwesomeIcon icon={faEye} className="me-2" /> View Details
              </Dropdown.Item>
              <Dropdown.Item>
                <FontAwesomeIcon icon={faEdit} className="me-2" /> Edit
              </Dropdown.Item>
              <Dropdown.Item className="text-danger">
                <FontAwesomeIcon icon={faTrashAlt} className="me-2" /> Remove
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </td>
      </tr>
    );
  };

  return (
    <Card border="light" className="table-wrapper table-responsive shadow-sm">
      <Card.Body className="pt-0">
        <Table hover className="user-table align-items-center">
          <thead>
            <tr>
              <th className="border-bottom">ID</th>
              <th className="border-bottom">Receipient</th>
              <th className="border-bottom">Placed at</th>
              <th className="border-bottom">Total</th>
              <th className="border-bottom">Status</th>
              <th className="border-bottom">Payment Method</th>
              <th className="border-bottom">ABA Transaction ID</th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(merchantOrders).length > 0 &&
              merchantOrders.orders.data.map((t) => (
                <TableRow key={`orders-${t.id}`} {...t} />
              ))}
          </tbody>
        </Table>
        <Card.Footer className="px-3 border-0 d-lg-flex align-items-center justify-content-between">
          <Nav>
            <Pagination className="mb-2 mb-lg-0">
              {Object.keys(merchantOrders).length > 0 &&
                merchantOrders.orders.links.map((link) => {
                  if (link.label.toLowerCase().includes("next")) {
                    return (
                      <Pagination.Next
                        href={
                          link.url
                            ? `${pathname}?${link.url.split("?")[1]}`
                            : null
                        }
                        disabled={link.url === null}
                        active={link.active}
                      >
                        Next
                      </Pagination.Next>
                    );
                  } else if (link.label.toLowerCase().includes("prev")) {
                    return (
                      <Pagination.Prev
                        href={
                          link.url
                            ? `${pathname}?${link.url.split("?")[1]}`
                            : null
                        }
                        disabled={link.url === null}
                        active={link.active}
                      >
                        Previous
                      </Pagination.Prev>
                    );
                  } else {
                    return (
                      <Pagination.Item
                        href={
                          link.url
                            ? `${pathname}?${link.url.split("?")[1]}`
                            : null
                        }
                        active={link.active}
                        disabled={link.url === null}
                      >
                        {link.label}
                      </Pagination.Item>
                    );
                  }
                })}
              {/* <Pagination.Prev>Previous</Pagination.Prev>
              <Pagination.Item>2</Pagination.Item>
              <Pagination.Item>3</Pagination.Item>
              <Pagination.Item>4</Pagination.Item>
              <Pagination.Item>5</Pagination.Item>
              <Pagination.Next>Next</Pagination.Next> */}
            </Pagination>
          </Nav>
          <small className="fw-bold">
            {/* Showing <b>{totalTransactions}</b> out of <b>25</b> entries */}
          </small>
        </Card.Footer>
      </Card.Body>
    </Card>
  );
};
