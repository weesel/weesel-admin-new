import React from "react";
import { Route, useRouteMatch, Redirect } from "react-router-dom";
import Dashboard from "./Dashboard";
import Product from "./Product";
import Order from "./Order";
import CreateProduct from "./Product/CreateProduct";

export default () => {
  const { path } = useRouteMatch();

  return (
    <React.Fragment>
      <Route exact path={path}>
        <Redirect to={`${path}/dashboard`} />
      </Route>
      <Route path={`${path}/dashboard`}>
        <Dashboard />
      </Route>
      <Route path={`${path}/orders`}>
        <Order />
      </Route>
      <Route exact path={`${path}/products`}>
        <Product />
      </Route>
      <Route path={`${path}/products/create`}>
        <CreateProduct />
      </Route>
    </React.Fragment>
  );
};
