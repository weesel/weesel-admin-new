import React, { useEffect } from "react";
import { Route, useRouteMatch, Redirect } from "react-router-dom";

export default () => {
  const { path, url } = useRouteMatch();
  useEffect(() => {
    console.log("path: ", path);
    console.log("url: ", url);
  }, []);
  return (
    <React.Fragment>
      <Route exact path={path}>
        <Redirect to={`${path}/dashboard`} />
      </Route>
      <Route path={`${path}/dashboard`}>
        <h1>Admin Dashboard</h1>
      </Route>
    </React.Fragment>
  );
};
