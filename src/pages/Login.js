import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { clearErrorMessage, selectAuthErrorMessage } from "../slices/auth";
import { loginWithRedirect } from "../slices/auth";

import BgImage from "../assets/img/illustrations/signin.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleLeft,
  faEnvelope,
  faUnlockAlt,
} from "@fortawesome/free-solid-svg-icons";
import {
  faFacebookF,
  faGithub,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import {
  Col,
  Row,
  Form,
  Card,
  Button,
  FormCheck,
  Container,
  InputGroup,
} from "@themesberg/react-bootstrap";

function Login() {
  const dispatch = useDispatch();
  const history = useHistory();

  const errorMessage = useSelector(selectAuthErrorMessage);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const onSubmitHandler = (e) => {
    e.preventDefault();
    dispatch(
      loginWithRedirect({
        loginCredentials: formData,
        redirectCallback: (url) => history.push(url),
      })
    );
  };

  const onChangeHandler = (e) => {
    setFormData((prevState) => {
      prevState[e.target.name] = e.target.value;
      return prevState;
    });
  };

  /**
   * error message
   */
  useEffect(() => {
    var clearInterval;
    if (errorMessage.header.length > 0 && errorMessage.content.length > 0) {
      clearInterval = setTimeout(() => {
        dispatch(clearErrorMessage());
      }, 4000);
    }
    return () => clearTimeout(clearInterval);
  }, [errorMessage, dispatch]);

  return (
    <main>
      <section className="d-flex align-items-center my-5 mt-lg-6 mb-lg-5">
        <Container>
          <Row
            className="justify-content-center form-bg-image"
            style={{ backgroundImage: `url(${BgImage})` }}
          >
            <Col
              xs={12}
              className="d-flex align-items-center justify-content-center"
            >
              <div className="bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                <div className="text-center text-md-center mb-4 mt-md-0">
                  <h3 className="mb-0">Sign in to manage our platform</h3>
                </div>
                <Form onSubmit={onSubmitHandler} className="mt-4">
                  <Form.Group id="email" className="mb-4">
                    <Form.Label>Your Email</Form.Label>
                    <InputGroup>
                      <InputGroup.Text>
                        <FontAwesomeIcon icon={faEnvelope} />
                      </InputGroup.Text>
                      <Form.Control
                        name="email"
                        onChange={onChangeHandler}
                        autoFocus
                        required
                        type="email"
                        placeholder="example@company.com"
                      />
                    </InputGroup>
                  </Form.Group>
                  <Form.Group>
                    <Form.Group id="password" className="mb-4">
                      <Form.Label>Your Password</Form.Label>
                      <InputGroup>
                        <InputGroup.Text>
                          <FontAwesomeIcon icon={faUnlockAlt} />
                        </InputGroup.Text>
                        <Form.Control
                          name="password"
                          onChange={onChangeHandler}
                          required
                          type="password"
                          placeholder="Password"
                        />
                      </InputGroup>
                    </Form.Group>
                  </Form.Group>
                  <Button variant="primary" type="submit" className="w-100">
                    Sign in
                  </Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </main>
  );
}

export default Login;
